const express = require('express') //inisiasi variable yang berisi express
const router = express.Router() // inisiasi variable yang berisi fungsi router express
const { list, create, update, destroy } = require('../controllers/categoryController') // inisiasi object controller
const validate = require('../middleware/validate')
const { createCategoryRules } = require('../validators/rule')
const checkToken = require('../middleware/checkToken')

router.get('/list', checkToken, list) // route untuk endpoint list
router.post('/create', checkToken, validate(createCategoryRules), create) // route untuk endpoint create
router.put('/update', checkToken, update) // route untuk endpoint update
router.delete('/destroy', checkToken, destroy) // route untuk endpoint destroy

module.exports = router // export fungsi router agar module lain bisa membaca file ini