'use strict';
module.exports = (sequelize, DataTypes) => {
    class permissions extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            this.belongsTo(models.role)
        }
    }
    permissions.init({
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        module: {
            type: DataTypes.STRING,
            allowNull: false
        },
        category_id: DataTypes.INTEGER
    }, {
        sequelize,
        modelName: 'permissions',
    });
    return permissions;
};